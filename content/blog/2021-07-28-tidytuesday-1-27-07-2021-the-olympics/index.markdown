---
title: 'TidyTuesday #1 - 27/07/2021 - The Olympics '
author: Clément Belliard
date: '2021-07-27'
slug: []
categories:
  - R
  - TidyTuesday
tags:
  - Dataviz
  - Learn
  - R Markdown
subtitle: ''
excerpt: ''
series: ~
layout: single
---




**Travail en cours**

## Init

```r
## Packages
library(tidyverse)

## Theme
theme_set(theme_minimal(base_family = "Arial", base_size = 13))

theme_update(
  plot.margin = margin(25,15,25,15)
  )
```

## Data


```r
olympics <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2021/2021-07-27/olympics.csv')
```


```r
tidy_olymp <- olympics %>% 
  mutate(across(where(is.character), factor))

glimpse(tidy_olymp)
```

```
## Rows: 271,116
## Columns: 15
## $ id     <dbl> 1, 2, 3, 4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, ~
## $ name   <fct> "A Dijiang", "A Lamusi", "Gunnar Nielsen Aaby", "Edgar Lindenau~
## $ sex    <fct> M, M, M, M, F, F, F, F, F, F, M, M, M, M, M, M, M, M, M, M, M, ~
## $ age    <dbl> 24, 23, 24, 34, 21, 21, 25, 25, 27, 27, 31, 31, 31, 31, 33, 33,~
## $ height <dbl> 180, 170, NA, NA, 185, 185, 185, 185, 185, 185, 188, 188, 188, ~
## $ weight <dbl> 80, 60, NA, NA, 82, 82, 82, 82, 82, 82, 75, 75, 75, 75, 75, 75,~
## $ team   <fct> "China", "China", "Denmark", "Denmark/Sweden", "Netherlands", "~
## $ noc    <fct> CHN, CHN, DEN, DEN, NED, NED, NED, NED, NED, NED, USA, USA, USA~
## $ games  <fct> 1992 Summer, 2012 Summer, 1920 Summer, 1900 Summer, 1988 Winter~
## $ year   <dbl> 1992, 2012, 1920, 1900, 1988, 1988, 1992, 1992, 1994, 1994, 199~
## $ season <fct> Summer, Summer, Summer, Summer, Winter, Winter, Winter, Winter,~
## $ city   <fct> Barcelona, London, Antwerpen, Paris, Calgary, Calgary, Albertvi~
## $ sport  <fct> Basketball, Judo, Football, Tug-Of-War, Speed Skating, Speed Sk~
## $ event  <fct> "Basketball Men's Basketball", "Judo Men's Extra-Lightweight", ~
## $ medal  <fct> NA, NA, NA, Gold, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, N~
```

Les premières idées qui me viennent en tête : 

 - Regarde par genre, le nombre de médaille par pays ;
 - Mettre en avant les pays outsider sur un épreuve. Par exemple, l'année où la finlande à gagner alors que les US emportent la médaille à chaque fois ;
 - En bon *frenchy*, faire un focus France sur les JO.
 


```r
tidy_olymp %>% 
  filter(!is.na(medal)) %>%
  filter( year >= 1950) %>% 
  filter(season == "Summer") %>%
  mutate(highlignt = ifelse(noc == "FRA", "FRA", "Other")) %>% 
  group_by(year, noc, highlignt) %>% 
  summarise(count = n()) %>% 
  ggplot(aes(year, count, group = noc, color = highlignt)) +
  geom_line() + 
  scale_color_manual(values = c("#69b3a2", "lightgrey")) +
  scale_size_manual(values=c(2,0.2)) +
  theme(legend.position = "none")
```

<img src="{{< blogdown/postref >}}index_files/figure-html/initial exploring-1.png" width="672" />


## Plot


