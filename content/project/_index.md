---
author: The R Markdown Team @RStudio
cascade:
  show_author_byline: true
  show_comments: false
  show_post_date: true
  sidebar:
    show_sidebar_adunit: true
    text_link_label: View all projects
    text_link_url: /project/
description: Un jour, peut-être, cette page sera bien remplie. En attendant, faites un tour dans sur le blog !
layout: list-grid
show_author_byline: true
show_post_date: false
show_post_thumbnail: true
sidebar:
  author: Clément Belliard
  description: "Projects can be anything!\nCheck out the _index.md file in the /project
    folder \nto edit this content.\n"
  show_sidebar_adunit: false
  text_link_label: ""
  text_link_url: ""
  title: A Sidebar for Your Projects
title: Rien encore dans le portefolio
---

** No content for the project index. This file provides front matter for the blog including the layout and boolean options. **
