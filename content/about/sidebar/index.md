---
## Configure sidebar content in narrow column
author: "Clément Belliard"
role: "GeekR"
avatar_shape: rounded # circle, square, rounded, leave blank to exclude
show_social_links: true # specify social accounts in site config
audio_link_label: #"How to say my name" # leave blank to exclude
link_list_label: "Intérêts" # bookmarks, elsewhere, etc.
link_list:
- name: Paris
  url: https://www.timeout.fr/paris/actualites
- name: Cuisiner et bien manger
  url: http://www.foudecuisine.fr/
- name: La tech 
  url: https://createurstech.fr/
---

** index doesn't contain a body, just front matter above.
See about/list.html in the layouts folder **