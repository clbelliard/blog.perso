---
## Configure header of page
text_align_right: false
show_title_as_headline: false
headline: |
  Bonjour, je suis Clément. Bienvenue à vous !
---

<!-- this is a subheadline -->
Vous êtes sur mon blog personnel. 

Je suis un français :fr: qui souhaite apprendre et partager son intérêt pour la dataviz et R en général. J'ai eu l'occasion de découvrir R dans le contexte professionnel et j'ai eu envi d'aller plus loin avec ce langage.  Ce blog est à la fois un nouveau projet sur R (apprendre à utiliser `blogdown`), une manière de capitaliser et partager mes acquis et un prétexte pour être plus constant dans l'apprentissage de R.

Dans ce blog, j'aimerai dans un premier temps partager mes participations futures au `TidyTuesday`, faire des articles où je collecte les ressources glânées ces derniers jours mais je l'espère plus de chose encore.

Bonne visite :smile: !