---
action_label: En savoir plus &rarr;
action_link: /about
action_type: text
description: 
image_left: true
images:
- img/revoir.jpg
show_action_link: true
show_social_links: true
subtitle: Dataviz et R
text_align_left: false
title: Blog de Clément B.
type: home
---

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
